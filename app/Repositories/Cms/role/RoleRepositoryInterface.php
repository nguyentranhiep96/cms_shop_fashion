<?php

namespace App\Repositories\Cms\role;

interface RoleRepositoryInterface
{
    public function findByRole();

    public function save(array $data, int $id = null);

    public function searchRole($data);
}
