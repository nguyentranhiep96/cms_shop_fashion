<?php

namespace App\Repositories\Cms\role;

use App\Models\Role;
use App\Repositories\BaseRepository;
use App\Repositories\Cms\role\RoleRepositoryInterface;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    public function model()
    {
        return Role::class;
    }

    public function findByRole()
    {
        return parent::query()->where('role_level', 1)->where('role_status', 1);
    }

    public function save(array $data, int $id = null)
    {
        return parent::save([
            'role_name' => $data['role_name'],
            'role_label' => $data['role_label'],
            'role_description' => $data['role_description'],
            'role_status' => $data['role_status'],
            'created_by' => 'System Admin',
        ], $id);
    }

    public function searchRole($data)
    {
        return parent::query()->where('role_level', 1)->where('role_label', 'like', '%' . $data . '%')->orderBy('id', 'DESC')->paginate(10);
    }

}
