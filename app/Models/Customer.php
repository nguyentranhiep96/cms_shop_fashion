<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $timestamps = true;

    protected $table = 'customers';

    protected $fillable = [
        'customer_name',
        'customer_code',
        'customer_email',
        'customer_phone',
        'customer_address',
        'customer_birthday',
        'customer_gender',
        'user_practise',
    ];
}
