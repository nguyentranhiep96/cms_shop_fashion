<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    public $timestamps = true;

    protected $table = 'product_groups';

    protected $fillable = [
        'stock_name',
        'stock_description',
        'user_practise'
    ];

    public function Products()
    {
        return $this->belongsToMany('App\Models\Product::class', 'product_store', 'store_id', 'product_id');
    }
}
